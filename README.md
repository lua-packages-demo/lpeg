# lpeg

Pattern-matching based on Parsing Expression Grammars (PEGs). https://luarocks.org/modules/gvvaughan/lpeg

## Unofficial documentation
* [*An introduction to Parsing Expression Grammars with LPeg*
  ](http://leafo.net/guides/parsing-expression-grammars.html)
  2015-07
